var gulp = require('gulp');
var sass = require('gulp-sass');
var fileinclude = require('gulp-file-include');

gulp.task('sass', function(){
	return gulp.src('style.sass')
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(gulp.dest('./'));
});

gulp.task('html', function(){
	gulp.src('templates/*.html')
		.pipe(fileinclude({
			prefix: '@@'
		}))
		.pipe(gulp.dest('./'))
});

gulp.task('default', ['sass', 'html']);

gulp.task('watch', ['sass', 'html'], function(){
	gulp.watch('*.sass', ['sass']);
	gulp.watch('templates/**/*.html', ['html']);
});
